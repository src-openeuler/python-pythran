%global _empty_manifest_terminate_build 0
Name:           python-pythran
Version:        0.17.0
Release:        1
Summary:        An ahead of time compiler for a subset of the Python language.
License:        BSD-3-Clause
URL:            https://github.com/serge-sans-paille/pythran
Source0:        https://files.pythonhosted.org/packages/source/p/pythran/pythran-%{version}.tar.gz
BuildArch:      noarch
%description
An ahead of time compiler for a subset of the Python language, with a focus on scientific computing.

%package -n python3-pythran
Summary:        An ahead of time compiler for a subset of the Python language.
License:        BSD-3-Clause
Provides:       python-pythran
# Base build requires
BuildRequires:  python3-devel
BuildRequires:  python3-setuptools
BuildRequires:  python3-pbr
BuildRequires:  python3-pip
BuildRequires:  python3-wheel
%description -n python3-pythran
An ahead of time compiler for a subset of the Python language, with a focus on scientific computing.

%package help
Summary:        Development documents and examples for pythran.
Provides:       python3-pythran-doc
%description help
Development documents and examples for pythran.

%prep
%autosetup -n pythran-%{version}
sed -i 's/\~=/\>=/g' requirements.txt

%build
%py3_build

%install
%py3_install

install -d -m755 %{buildroot}/%{_pkgdocdir}
if [ -d doc ]; then cp -arf doc %{buildroot}/%{_pkgdocdir}; fi
if [ -d docs ]; then cp -arf docs %{buildroot}/%{_pkgdocdir}; fi
if [ -d example ]; then cp -arf example %{buildroot}/%{_pkgdocdir}; fi
if [ -d examples ]; then cp -arf examples %{buildroot}/%{_pkgdocdir}; fi
pushd %{buildroot}
if [ -d usr/lib ]; then
    find usr/lib -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/lib64 ]; then
    find usr/lib64 -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/bin ]; then
    find usr/bin -type f -printf "/%h/%f\n" >> filelist.lst
fi
if [ -d usr/sbin ]; then
    find usr/sbin -type f -printf "/%h/%f\n" >> filelist.lst
fi
touch doclist.lst
if [ -d usr/share/man ]; then
    find usr/share/man -type f -printf "/%h/%f.gz\n" >> doclist.lst
fi
popd
mv %{buildroot}/filelist.lst .
mv %{buildroot}/doclist.lst .

%files -n python3-pythran -f filelist.lst

%dir %{python3_sitelib}/*

%files help -f doclist.lst
%{_docdir}/*

%changelog
* Thu Nov 07 2024 caiyuxin <caiyuxin@kylinos.cn> - 0.17.0-1
- Update package to version 0.17.0
- Add scipy-openblas support
- Bump gast requirement to 0.6.0
- Fix numpy.fix output type
- Drop legacy numpy support for blas detection
- Improve error when input is not valid Python

* Tue Aug 20 2024 dongjiao <dongjiao@kylinos.cn> - 0.16.1-1
- Update version to 0.16.1
  - Numpy 2.x support
  - Support python 3.12

* Thu Nov 30 2023 Dongxing Wang <dongxing.wang_a@thundersoft.com> - 0.14.0-1
- Init package
